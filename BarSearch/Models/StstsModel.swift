//
//  StstsModel.swift
//  BarSearch
//
//  Created by Vladimir on 19/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation

class StatsModel: NSObject, Decodable {
    var tipCount: Int?
    var usersCount: Int?
    var checkinsCount: Int?
}
