//
//  LocationModel.swift
//  BarSearch
//
//  Created by Vladimir on 19/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation

class LocationModel: NSObject, Decodable {
    var address: String?
    var lat: Double?
    var lng: Double?
    var distance: Int?
    var city: String?
    var country: String?
}
