//
//  ContactModel.swift
//  BarSearch
//
//  Created by Vladimir on 18/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation

class ContactModel: NSObject, Decodable {
    var phone: String?
    var formatedPhone: String?
}
