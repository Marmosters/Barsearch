//
//  LocationMO+CoreDataProperties.swift
//  BarSearch
//
//  Created by Vladimir on 18/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//
//

import Foundation
import CoreData

extension LocationMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LocationMO> {
        return NSFetchRequest<LocationMO>(entityName: "LocationMO")
    }

    @NSManaged public var address: String?
    @NSManaged public var lat: Double
    @NSManaged public var lng: Double
    @NSManaged public var distance: NSNumber?
    @NSManaged public var city: String?
    @NSManaged public var country: String?
    @NSManaged public var venue: BarModelMO?

}
