//
//  StatsMO+CoreDataProperties.swift
//  BarSearch
//
//  Created by Vladimir on 18/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//
//

import Foundation
import CoreData

extension StatsMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StatsMO> {
        return NSFetchRequest<StatsMO>(entityName: "StatsMO")
    }

    @NSManaged public var tipCount: NSNumber
    @NSManaged public var usersCount: NSNumber
    @NSManaged public var checkinsCount: NSNumber
    @NSManaged public var venue: BarModelMO?

}
