//
//  BarModelMO+CoreDataProperties.swift
//  BarSearch
//
//  Created by Vladimir on 18/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//
//

import Foundation
import CoreData

extension BarModelMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BarModelMO> {
        return NSFetchRequest<BarModelMO>(entityName: "BarModelMO")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var contact: ContactMO?
    @NSManaged public var location: LocationMO?
    @NSManaged public var stats: StatsMO?

}
