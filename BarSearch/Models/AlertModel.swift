//
//  AlertModel.swift
//  BarSearch
//
//  Created by Vladimir on 20/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation

struct AlertModel {
    var title: String
    var message: String

    init(title: String, message: String) {
        self.title = title
        self.message = message
    }
}
