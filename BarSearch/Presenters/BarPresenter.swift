//
//  BarPresenter.swift
//  BarSearch
//
//  Created by Vladimir on 17/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation
import CoreLocation

class BarPresenter: NSObject {

    private let locationManager: LocationManager
    private let barManager: BarManager
    var currentLocation: CLLocationCoordinate2D? {
        didSet {
            view?.loationHasUpdated()
        }
    }
    private let searchRadius = 5000
    var view: BarSearchView?
    var barList: [BarModel]? {
        didSet {
            view?.barListReadyToPresent()
        }
    }

    override init() {
        locationManager = LocationManager.shared
        barManager = BarManager.shared
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(barListUpdated(_:)), name: Notification.Name.barListUpdatedNotification, object: nil)
        locationManager.delegate = self
        locationManager.requestLocation()
    }

    @objc private func barListUpdated(_ notification: Notification) {
        if let list = notification.userInfo?[UserInfo.bars] as? [BarModel] {
            barList = list
            if list.isEmpty {
                let alert = AlertModel(title: BSStrings.notFoundTitle, message: BSStrings.notFoundMessage)
                NotificationCenter.default.post(name: Notification.Name.showAlertNotification, object: nil, userInfo: [UserInfo.alert: alert])
            }
        }

    }

    func searchBars() {
        if let location = currentLocation {
            view?.searchStarted()
            barManager.getBars(near: location, radius: searchRadius)
        } else {
          locationManager.requestLocation()
        }
    }

    func updateBars() {
        view?.searchStarted()
        locationManager.requestLocation()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

extension BarPresenter: LocationReciver {
    func newLocationRecived(_ location: CLLocationCoordinate2D?) {
        currentLocation = location
        searchBars()
    }

}
