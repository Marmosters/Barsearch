//
//  NetworkManager.swift
//  BarSearch
//
//  Created by Vladimir on 17/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

class BarManager: NSObject {
    static let shared = BarManager()

    let context = AppDelegate.shared.persistentContainer.viewContext

    private override init() {
        super.init()
    }

    func getBars(near location: CLLocationCoordinate2D, radius: Int) {
        NetworkReachability.isConnected ? getBarsFromNetwork(near: location, radius: radius) : getBarsFromDatabase()

    }

    private func getBarsFromDatabase() {
        let alert = AlertModel(title: BSStrings.noConnectionTitle, message: BSStrings.noConnectionMessage)

        NotificationCenter.default.post(name: Notification.Name.showAlertNotification, object: nil, userInfo: [UserInfo.alert: alert])
        var requset = NSFetchRequest<NSFetchRequestResult>(entityName: BSEEntities.barModelMO)
        let context = AppDelegate.shared.persistentContainer.viewContext
        let barsMO = try? context.fetch(requset)
        var bars = [BarModel]()
        for item in barsMO! {
            if let bar = item as? BarModelMO {
                let predicate = NSPredicate(format: "venue = %@", bar)
                requset = NSFetchRequest<NSFetchRequestResult>(entityName: BSEEntities.locationMO)
                requset.predicate = predicate
                if let location = ((try? context.fetch(requset).first as? LocationMO) as LocationMO??) {
                    bar.location = location
                }
                requset = NSFetchRequest<NSFetchRequestResult>(entityName: BSEEntities.contactMO)
                requset.predicate = predicate
                if let contact = ((try? context.fetch(requset).first as? ContactMO) as ContactMO??) {
                    bar.contact = contact
                }
                requset = NSFetchRequest<NSFetchRequestResult>(entityName: BSEEntities.statsMO)
                requset.predicate = predicate
                if let stats = ((try? context.fetch(requset).first as? StatsMO) as StatsMO??) {
                    bar.stats = stats
                }

                bars.append(bar.barModel)
            }
        }
        let sortedBars = bars.sorted(by: {$0.stats?.tipCount ?? 0 > $1.stats?.tipCount ?? 0})
        NotificationCenter.default.post(name: Notification.Name.barListUpdatedNotification, object: nil, userInfo: [UserInfo.bars: sortedBars])

    }

    private func getBarsFromNetwork(near location: CLLocationCoordinate2D, radius: Int) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in

            let api = BarSearchGetAPI()
            api.location = location
            api.radius = radius

            api.performRequest(compleation: { result in
                if let bars = result {
                    let dispatcGroup = DispatchGroup()
                    for bar in bars {
                        dispatcGroup.enter()
                        let statsApi = GetVenueStstsAPI()
                        statsApi.id = bar.id
                        statsApi.performRequest { stats in
                            bar.stats = stats
                            dispatcGroup.leave()
                        }
                    }
                    dispatcGroup.wait()
                    DispatchQueue.main.async {
                        let sortedBars = bars.sorted(by: {$0.stats?.tipCount ?? 0 > $1.stats?.tipCount ?? 0})
                        let userInfo = [UserInfo.bars: sortedBars]
                        NotificationCenter.default.post(name: Notification.Name.barListUpdatedNotification, object: nil, userInfo: userInfo)
                        self?.clearData()
                        for bar in sortedBars {
                            self?.saveBar(bar)
                        }
                    }
                }
            })
        }
    }

    private func clearData() {

            let request = NSFetchRequest<NSFetchRequestResult>(entityName: BSEEntities.barModelMO)
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
            do {
                try self.context.execute(deleteRequest)
                try self.context.save()
            } catch { }
    }

    private func saveBar(_ bar: BarModel) {
        let context = AppDelegate.shared.persistentContainer.viewContext
        guard let barMO =  NSEntityDescription.insertNewObject(forEntityName: BSEEntities.barModelMO, into: context) as? BarModelMO else { return }
        barMO.id = bar.id
        barMO.name = bar.name
        barMO.contact = (NSEntityDescription.insertNewObject(forEntityName: BSEEntities.contactMO, into: context) as? ContactMO)
        barMO.contact?.phone = bar.contact?.phone
        barMO.contact?.formatedPhone = bar.contact?.formatedPhone
        barMO.location = (NSEntityDescription.insertNewObject(forEntityName: BSEEntities.locationMO, into: context) as? LocationMO)
        barMO.location?.address = bar.location?.address
        barMO.location?.lat = bar.location?.lat ?? 0
        barMO.location?.lng = bar.location?.lng ?? 0
        barMO.location?.distance = bar.location?.distance as NSNumber? ?? 0
        barMO.location?.country = bar.location?.country
        barMO.location?.city = bar.location?.city
        barMO.stats = (NSEntityDescription.insertNewObject(forEntityName: BSEEntities.statsMO, into: context) as? StatsMO)
        barMO.stats?.tipCount = bar.stats?.tipCount as NSNumber? ?? 0
        barMO.stats?.usersCount = bar.stats?.usersCount as NSNumber? ?? 0
        barMO.stats?.checkinsCount = bar.stats?.checkinsCount as NSNumber? ?? 0
        do {
            try context.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }

}

struct ResponceObject: Decodable {
    var response: Responce
}

struct Responce: Decodable {
    var venues: [BarModel]
}
