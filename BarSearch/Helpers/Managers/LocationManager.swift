//
//  LocationManager.swift
//  BarSearch
//
//  Created by Vladimir on 17/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject {

    static let shared = LocationManager()
    private let manager: CLLocationManager
    weak var delegate: LocationReciver?

    private override init() {
        manager = CLLocationManager()
        super.init()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
    }

    func requestLocation() {
        manager.requestLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate?.newLocationRecived(manager.location?.coordinate)
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}
