//
//  Strings.swift
//  BarSearch
//
//  Created by Vladimir on 17/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation

class UserInfo {
    static let bars = "bars"
    static let alert = "alert"
}

class ReuseIdentifers {
    static let barCell = "barCell"
    static let barAnnotationView = "barAnnotationView"
    static let toMapSegue = "toMap"
}

class BSStrings {
    static let checkins  = NSLocalizedString("checkins", comment: "Checkinn")
    static let distance =  NSLocalizedString("distance", comment: "Distance")
    static let listViewTitle = NSLocalizedString("listView.Title", comment: "Bars list")
    static let mapViewTitle = NSLocalizedString("mapView.Title", comment: "Bars map")
    static let ok = NSLocalizedString("ok", comment: "OK")
    static let noConnectionTitle = NSLocalizedString("noConnection.title", comment: "No internet connection")
    static let noConnectionMessage = NSLocalizedString("noConnection.message", comment: "Previosly storred bars will be displayed")
    static let notFoundTitle = NSLocalizedString("notFound.title", comment: "Nothing to display")
    static let notFoundMessage = NSLocalizedString("notFound.message", comment: "No data found")
    static let error = NSLocalizedString("error", comment: "Error")
}

class BSImages {
    static let mapIcon = "map_icon"
    static let barPin = "bar_pin"
}

class BSEEntities {
    static let barModelMO = "BarModelMO"
    static let contactMO = "ContactMO"
    static let locationMO = "LocationMO"
    static let statsMO = "StatsMO"
}
