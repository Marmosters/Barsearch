//
//  BaseAPI.swift
//  BarSearch
//
//  Created by Vladimir on 19/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation

class BaseAPI {

    var parameters: [String: String] {
        var parameters = [String: String]()
        parameters["client_id"] =  "COGVSCDR3CCV55KWS0VT2AJIQROHDRBP0RIG0Q2GU4BDTH4O"/* "EKBWVCUL1HOZFYN1ND42EWV1ZJSMG0DF0WNMWPGYYZ4OEVTT" "MNHPTTE5J1SAG54YQPCKFD4OFMQX4OZPALNHTODBO2GNYC5J"*/
        parameters["client_secret"] = "OLQFXSSGS3YU5WCO112QQ5LBPHOB4Y2GYVIHJUHHGGMX0QSE"/* "0R1AKATL0ABRPHKSX3AGGX1H4VHDQVWACEQEGJPNJTFSFWYX" "0HY3Y2MBSJZTBP1RJQ4WZFAMQVALQXNROF3QZMCFHLYD0Y3D"*/
        parameters["v"] = "20180418"
        return parameters
    }

    var baseUrl: String {
        return "https://api.foursquare.com/v2"
    }

    var path: String {
        preconditionFailure("Request path should be set, override path")
    }

    var url: String {
        var url = "\(baseUrl)\(path)?"
        var isFirst = true
        for parameter in parameters {
            if !isFirst {
                url += "&"
            } else {
                isFirst = false
            }
            url += "\(parameter.key)=\(parameter.value)"
        }
        return url
    }

}
