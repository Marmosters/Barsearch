//
//  GetVenueStstsAPI.swift
//  BarSearch
//
//  Created by Vladimir on 19/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation

struct DetailsResponceObject: Decodable {
    var response: DetailsResponce
}

struct DetailsResponce: Decodable {
    var venue: Venue
}

struct Venue: Decodable {
    var stats: StatsModel
}

class GetVenueStstsAPI: BaseAPI {
    var id: String!

    override var path: String {
        return "/venues/\(id!)"
    }

    func performRequest(compleation: ((StatsModel?) -> Void)?) {
        let task = URLSession.shared.dataTask(with: NSURL(string: url)! as URL, completionHandler: {(data, response, error) -> Void in
            let decoder = JSONDecoder()
            var stats: StatsModel?


            do {
                let result = try decoder.decode(DetailsResponceObject.self, from: data!)
                stats = result.response.venue.stats
                compleation!(stats)

            } catch {
                let alert = AlertModel(title: BSStrings.error, message: error.localizedDescription)
                NotificationCenter.default.post(name: Notification.Name.showAlertNotification, object: nil, userInfo: [UserInfo.alert: alert ])
                compleation!(stats)
            }

        })
        task.resume()
    }

}
