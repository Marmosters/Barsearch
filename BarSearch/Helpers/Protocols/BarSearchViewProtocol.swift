//
//  BarSearchViewProtocol.swift
//  BarSearch
//
//  Created by Vladimir on 18/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import Foundation

protocol BarSearchView {
    func barListReadyToPresent()
    func searchStarted()
    func loationHasUpdated()
}

extension BarSearchView {
    func loationHasUpdated() {}
    func searchStarted() {}
}
