//
//  BaseViewController.swift
//  BarSearch
//
//  Created by Vladimir on 20/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(showAlertMessage(_:)), name: Notification.Name.showAlertNotification, object: nil)
    }

    @objc private func showAlertMessage(_ notification: Notification) {
        DispatchQueue.main.async {
            guard !(self.presentedViewController is UIAlertController) else { return }
            if let alert = notification.userInfo?[UserInfo.alert] as? AlertModel {
                let alert = UIAlertController(title: alert.title, message: alert.message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: BSStrings.ok, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}
