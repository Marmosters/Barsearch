//
//  BarTableViewCell.swift
//  BarSearch
//
//  Created by Vladimir on 18/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import UIKit

class BarTableViewCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var checkinsLabel: UILabel!
    @IBOutlet private weak var distanceLabel: UILabel!

    var item: BarModel? {
        didSet {
            titleLabel.text = item?.name
            checkinsLabel.text = "\(BSStrings.checkins) \(item?.stats?.tipCount ?? 0)"
            distanceLabel.text = "\(BSStrings.distance) \(item?.location?.distance ?? 0)"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
