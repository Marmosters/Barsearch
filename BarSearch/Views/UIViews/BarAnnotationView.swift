//
//  BarAnnotationView.swift
//  BarSearch
//
//  Created by Vladimir. Evstratov on 11/19/18.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import UIKit
import MapKit

class BarAnnotationView: MKAnnotationView {
    private var titleLabel: UILabel!
    var title: String? {
        get {
            return titleLabel.text
        }
        set {
           titleLabel.text = newValue
        }
    }

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        isEnabled = true
        canShowCallout = false
        image = UIImage.init(imageLiteralResourceName: BSImages.barPin)
        titleLabel = UILabel(frame: CGRect(x: 5, y: 32.0, width: 150.0, height: 16.5))
        titleLabel.font = UIFont(name: "HelveticaNeue", size: 16.0)
        titleLabel.textAlignment = .left
        titleLabel.textColor = UIColor.black
        titleLabel.adjustsFontSizeToFitWidth = true
        addSubview(titleLabel)

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
