//
//  BarMapController.swift
//  BarSearch
//
//  Created by Vladimir on 17/11/2018.
//  Copyright © 2018 Vladimir. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class BarMapController: BaseViewController {

    @IBOutlet weak var mapView: MKMapView!
    let presenter = BarPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = BSStrings.mapViewTitle
        presenter.view = self
        updateLocation()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearBars()
        presentBars()
        super.viewWillAppear(animated)
    }

    private func updateLocation() {
        guard presenter.currentLocation != nil else {return}
        let region = MKCoordinateRegion(center: presenter.currentLocation!, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.region = region
    }

    private func presentBars() {
        if let bars = presenter.barList {
            mapView.addAnnotations(bars)
        }
    }

    private func clearBars() {
        for item in mapView.annotations {
            if case let item as BarModel = item {
                mapView.removeAnnotation(item)
            }
        }
    }

}

extension BarMapController: BarSearchView {

    func barListReadyToPresent() {
        clearBars()
        presentBars()
    }

    func loationHasUpdated() {
       updateLocation()
    }
}

extension BarMapController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationIdentifier = ReuseIdentifers.barAnnotationView
        guard !annotation.isKind(of: MKUserLocation.self) else {
            return nil
        }

        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        if annotationView == nil {
            annotationView = BarAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)

        }

        if case let annotationView as BarAnnotationView = annotationView {
            annotationView.annotation = annotation
            annotationView.title = (annotation as? BarModel)?.name ?? ""
        }

        return annotationView
    }
}
